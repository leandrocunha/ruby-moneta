#!/bin/sh
#
# Copyright (C) 2014 Thomas Goirand <zigo@debian.org>
#
# Runs pgsql server, then use that to run tests.
#

set -eu
set -x

PG_MYTMPDIR=${1}
PGSQL_PORT=${2}
BINDIR=`pg_config --bindir`

# depends on language-pack-en | language-pack-en
# because initdb acquires encoding from locale
export LC_ALL="C"
export LANGUAGE=C
${BINDIR}/initdb -D ${PG_MYTMPDIR}

${BINDIR}/pg_ctl -w -D ${PG_MYTMPDIR} -o "-k ${PG_MYTMPDIR} -p ${PGSQL_PORT} -c max_connections=500" start > /dev/null
attempts=0
while ! [ -e ${PG_MYTMPDIR}/postmaster.pid ] ; do
	attempts=$((attempts+1))
	if [ "${attempts}" -gt 10 ] ; then
		echo "Exiting test: postgres pid file was not created after 30 seconds"
		exit 1
	fi
	sleep 3
	echo `date`: retrying..
done

# Set the env. var so that pgsql client doesn't use networking
# libpq uses this for all host params if not explicitly passed
export PGHOST=${PG_MYTMPDIR}
export PGPORT=${PGSQL_PORT}

# Create a new test db
createuser --superuser moneta
createdb -O moneta moneta1
createdb -O moneta moneta2

# Create hstore extension
psql -c 'create extension hstore;' moneta1
